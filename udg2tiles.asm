	org $8B95  ; 35733

; Code for article breakdown Bob Papes pdf
; continues to second part of process tiling the UDG and UDG_map

;variables from last part

udg_store equ $82a5
udg_map equ $8025
attrib_map equ $d800

	jp make_tiles
	jp tile_show ; poke 44007 with tile ref 
	nop
	nop
	nop


temp_tile 
	ds 32, 0

tile_num   db 1 ; tile zero is all zeros and black paper and onk

tile_map 
	ds 40, 0 ; 8 x 5 tiled map for screen.

tile_store 
	ds 32*256,0 ; 255	 tiles


attrib equ $d800
select_tile dw $0000 ; b*256+c


make_tiles  ld bc,$0805 ; this is the along and down

make_loopb
	    push bc
make_loopc
	   push bc
; find the storage reference for the tile map

	    ld a,c		; storing data in row order
	    dec a
	    add a,a ; x2	
	    add a,a ; x4
	    add a,a ; x8
	    ld e,a
	    ld a,b
	    dec a
	    add a, e ; a = (c-1)*8 + (b-1)

	    ld (tile1+2),a; smc alters index of found tile ref store
	    ld (tile2+2),a  
;	    ld (tile3+2),a  

	    call get_Tile

; so we know what our tile should be and we know were it should be referenced int he tile map

	   ld de,temp_tile
	   ld hl,tile_Store
	   call findTile

; it should be found and stored in the tile map and a new refernce made if needed

	   pop bc
	   dec c
	   jr nz, make_loopc
	   pop bc
	   djnz make_loopb
	   ret


		


sel_Tile ;enter b = along, c = down in the udg store

	
	ld bc,(select_tile)
get_Tile  ; bc already set

	ld ix,temp_tile

	ld h,0
	ld l,c
	dec l
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl

	ex de,hl
	ld h,0
	ld l,b
	dec l
	add hl,hl
	add hl,hl ;4
	add hl,de; offset = (b-1)*4 + (c-1)*32*4

	push hl
	pop bc
	ld hl,	attrib ; picture colouts
	add hl,bc
	ex de,hl
	ld hl,udg_map
	add hl,bc

	ld bc,$0404

GT_loopb

	push hl
	push de
	push bc

GT_loopc
	push bc
	ld bc,32
	ld a,(hl)
	ld (ix+0),a
	ld a,(de)
	ld (ix+16),a
	ex de,hl
	add hl,bc
	ex de,hl
	add hl,bc
	inc ix
	pop bc
	dec c
	jr nz, GT_loopc
	pop bc
	pop de
	pop hl
	inc hl
	inc de
	djnz GT_loopb
	ret


findTile
; Entry
;de points to the temp tile
;hl points to the tile store

	ld c,0
	ld ix,tile_map

Tileloop push bc
	push de  
	push hl  
	ld h,0
	ld l,c
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl

	ld bc,Tile_store
	add hl,bc

	call compare_Tile

	jr z,match
	pop hl
	ld de,32
	add hl,de ;next Tile
	pop de
	pop bc
	inc c
	ld a,(Tile_num)
	cp c
	jr nz,Tileloop
; reached here so its a new one hl pointing to the store area
	
	call store_Tile
	
	ld a,(Tile_num)

tile1	ld (ix+0),a ; SMC code for storing tile ref

	inc a
	ld (Tile_num),a
	ret

match	pop hl
	pop de
	pop bc
tile2	ld (ix+0),c ; SMC code for storing tile ref
	ret




compare_Tile 
; exit nz = differnt ; z= the same

 
	ld b,32
comploop  ld a,(de)
          cp (hl)
	  ret nz ; it different
	  inc de
          inc hl
          djnz comploop ; its the same as thisone...
	  xor a
	  ret


store_Tile ld de,temp_tile
	ex de,hl
	ld bc,32
	ldir
	ret

; ***


tile_show	ld de,(select_tile)

		ld h,0
		ld l,e	
		add hl,hl
		add hl,hl
		add hl,hl
		add hl,hl
		add hl,hl
		ld bc,tile_Store
		add hl,bc
		ld de,temp_tile
	        ld bc,32
		ldir
		call disp_temp_tile
		ret

		

disp_temp_tile ld ix,temp_tile

disp_Tile ; ix already set

	  exx 

	push bc
	push hl
	push de
	  ld hl,$4000
	  ld de,$5800
	  exx

	  ld bc,$0404
dt_loopb
	  push bc
	  exx
	  push de
	  push hl
	  exx

dt_loopc
	  push bc
	  exx
	  push hl
	  exx
	  
	  ld a,(ix+0) ; udg offset

	  ld h,0
	  ld l,a
	  add hl,hl
	  add hl,hl
	  add hl,hl
	  ld de,udg_store
	  add hl,de     

	 ld b,8
byte_loop
	ld a,(hl)
	inc hl
	 exx
	ld (hl),a
	inc h
	exx
	djnz byte_loop

	ld a,(ix+16)
	exx
	ld (de),a
	pop hl
	
	ld bc,32	
        add hl,bc
        ex de,hl
	add hl,bc
	ex de,hl
	exx
	inc ix
	pop bc
	dec c
	jr nz,dt_loopc

	exx 
	pop hl
	pop de
	inc hl
	inc de
	exx
	pop bc
	djnz dt_loopb

	exx
	pop de
	pop hl
	pop bc
	exx

	ret






next	db 0

