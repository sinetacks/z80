 org $ad1e ; 44318
; level packager



udg_num equ $8024
udg_map equ $8025
udg_store equ $82A5

tile_num equ $8bbe
tile_map equ $8bbf
tile_store equ $8be7


output_file equ $DB00

	jp data_pack
	jp data_details


header db "T8x5"

string1 db 22,0,0,"Number of UDG   : ",$ff
string2 db 22,1,0,"Number of Tiles : ",$ff
string3 db 22,3,0,"Date store : ",$ff

printz  
; print zero terminated string at hl

	ld a,(hl)
	cp $ff
	ret z
	rst $10
	inc hl
	jp printz


printa
;print number stored in accum ; uses calc stack to give 0 - 65535

	ld b,0
	ld c,a

printbc
;print number stored in BC register uses ROM and calc stack to give 0 - 65535

       call 11563          ; stack number in bc.
       call 11747          ; display top of calc. stack.
	ret

printnumudg
	
	ld a,(udg_num)
	call printa
	ret

printnumtiles
	ld a,(tile_num)
	call printa
	ret

data_details

	ld a,2
	call $1601

	ld hl,string1
	call printz
	call printnumudg
	
	ld hl,string2
	call printz
	call printnumtiles

	ld hl,string3
	call printz
	ld bc,output_file
	call printbc
	ld a,","
	rst $10
datalen	ld bc,0  ; smc
	call printbc
	ret

	

data_pack
	call make_data
	ret



make_data

;	db "t8x5"
;	db 40 ; tile map length
;	db udg_num ; num of UDGs
;	db tile_num ; num of Tiles
;	ds 40,0 
;       ds udg_num*8
;       ds tile_num*32

	ld de,output_file
	ld hl,header
	ld bc,4
	ldir

	ld a,40
	ld (de),a
	inc de

	ld a,(udg_num)
	ld (de),a
	inc de
	ld a,(tile_num)
	ld (de),a
	inc de

	ld bc,40
	ld hl,tile_map
	ldir
	


	ld a,(udg_num)
	ld h,0
	ld l,a
	add hl,hl
	add hl,hl
	add hl,hl
	push hl
	pop bc
	ld hl,udg_store
	ldir

	ld a,(tile_num)
	ld h,0
	ld l,a
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	push hl
	pop bc
	ld hl,tile_store
	ldir

	ld hl,output_file
	ex de,hl
	xor a
	sbc hl,de


	ld (datalen+1),hl

	
	ret

next db 0
