	org $ae05 ; 44549


screen2store	equ $8003
makeudg_map	equ $8006
make_tiles	equ $ABE9  ; $8b95
make_data	equ $ad1e
data_Details	equ $ad21
printz		equ $ad65
printa		equ $ad6e
printbc		equ $ad71
tile_show	equ 35736 ; poke 44007 with tile wanted


		jp exec
		jp show_udg
		jp tile_show

		

exec		call load_screen
		call wait
		call screen2store
		call makeudg_map
		call make_tiles
		call make_data
		call data_Details
		ret

intro_text	db 22,0,0,"Load the picture to tile"
		db 22,1,0,"Press a key when loaded",$ff

loaded_text	db 22,0,0,"A picture is loaded"
		db 22,1,0,"Press a key to tile",$ff

control equ $bf ; flashing white on white as a control byte in last attrib for screen

load_screen	ld a,1
		call $1601
		ld hl,intro_text
		call printz
		ld a,control
		ld (attrib_end),a
		ret

attrib_end	equ $5aff

wait		ld a,(attrib_end)
		cp control
		jr z,wait
		ld hl,loaded_text
		call printz
	       	ld hl,23560         ; LAST K system variable.
 	      	ld (hl),0           ; put null value there.
loop   		ld a,(hl)           ; new value of LAST K.
	        cp 0                ; is it still zero?
	        jr z,loop           ; yes, so no key pressed.
 	        ret                 ; key was pressed.


output_file equ $DB00

;	db "t8x5"
;	db 40 ; tile map length
;	db udg_num ; num of UDGs
;	db tile_num ; num of Tiles
;	ds 40,0 
;       ds udg_num*8
;       ds tile_num*32


udg2disp  equ $ 8b32   ; point hl to a buffer with all the udg in the 30*20 format..
			; used to print out all the udgs from out saved data


udg_map equ $8025

buffer
	ds 640 , 0



show_udg	ld ix,output_file


		ld hl,buffer
		ld de, buffer+1
		ld bc,639
		ld (hl),0
		ldir		; clear buffer


		ld hl, buffer + 96
		ld b,(ix+5) ; udg_num
		ld c,0
udg_loop	ld (hl),c
		inc hl
		inc c
		djnz udg_loop

		ld hl,buffer
		call udg2disp

		ld hl,$5880
		ld bc, 16
		ld de,$3878
attr1_loop	
		ld (hl),e
		inc hl
		ld (hl),d
		inc hl
		dec bc
		ld a,b
		or c
		jr nz,attr1_loop

		ld bc, 16
		ld de,$7838
attr_loop	
		ld (hl),e
		inc hl
		ld (hl),d
		inc hl
		dec bc
		ld a,b
		or c
		jr nz,attr_loop

		ld hl,$5880
		ld de,$58c0
		ld bc, 64*8
		ldir

		call data_Details

		ret


next	db 0

