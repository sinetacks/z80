
	org $b184 ; 45444

tile_show	equ 35736 ; poke 44007 with tile wanted


; Input using Spectrum Rom Routines
; variation from old magazine article
;
; http://www.users.globalnet.co.uk/~jg27paw4/yr11/yr11_66.htm
 
 
LD HL,TEXT
CALL BASIC

call tile_show

RET

RST 8
DEFB 255 

BASIC 
PUSH HL
CALL 5808
LD HL,(23641)
LD BC,130
CALL 5717
INC HL
EX DE,HL
POP HL
LD BC,128
LDIR
SET 7,(IY+48)
LD (IY+0),255
LD (IY+10),1
LD HL,(23613)
PUSH HL
LD HL,BACK
PUSH HL
LD (23613),HL
CALL 7050
POP HL 
 

BACK 
POP HL
LD (23613),HL
CALL 5808
LD A,(23610)
CP 255
JP NZ,4867
RET 
TEXT 
  
db $ee,$22,"TILE Num: ",$22,$2c,$41,$3a,$f4,$B0,$22,"44007",$22,$20,$2C,$41 
; INPUT "Tile Num: ",A : POKE VAL "44007" , A  
DEFB 13 ; Terminate the string with carriage return. 


