	org $8000
; Code for thegrid blog on "Decoding the r-Type Diaries"
; Screen to Tile coding process 1st step - image to UDG and UDG map.
; the attribute map is already saved as part of the screen copy. 

;variables
screen	equ $4000
length equ 6144+698
store equ $c000
exec equ  $ae05; this is to call the single call routine


	jp exec ; move current screen to $c000
	jp screen2store ; display stored screen
	jp makeUDGs_map ; make UDG's from display 
	jp udgmap2disp ;  Test the output once converted.


store2screen
; save the loaded screen to $c000 for future use
	ld hl, store
	ld de,screen
	ld bc,length ; - 698
	ldir
	ret


screen2store
; copy save screen to $c000 for work
	ld hl, screen
	ld de, store
	ld bc,length ; - 698
	ldir
	ret



UDG_num 	db 1; / first defined is always a blank
UDG_map 	
	ds 640,0 ; store the characters as UDG references
UDG_store 	
	ds 8*256,0 ; 256 graphics worth


makeUDGs_map

	ld ix,UDG_map

	ld de,$4000 ; start of screen
	ld hl,UDG_store
	ld bc,0
	call loop_map
	ld de,$4800
	ld hl,UDG_store
	ld bc,0
	call loop_map
	ld de,$5000
	ld hl,UDG_store
	ld bc,$8000


loop_map
	push bc   ; on loop djnz
	push hl   ; on loop restore hl
	push de   ; on loop inc e and inc ix

	call findUDG

	pop de
	inc e
	inc ix
	pop hl
	pop bc
	djnz loop_map

	ret

findUDG

	ld c,0

UDGloop push bc
	push de
	push hl
	ld h,0
	ld l,c
	add hl,hl
	add hl,hl
	add hl,hl
	ld bc,UDG_store
	add hl,bc

	call compare_UDG
	jr z,match
	pop hl
	ld de,8
	add hl,de ;next UGD
	pop de
	pop bc
	inc c
	ld a,(UDG_num)
	cp c
	jr nz,UDGloop
; reached here so its a new one hl pointing to the store area
	
	call store_UDG
	
	ld a,(UDG_num)
	ld (ix+0),a
	inc a
	ld (UDG_num),a
	ret

match	pop hl
	pop de
	pop bc
	ld (ix+0),c
	ret



compare_UDG

; exit nz = differnt ; z= the same

; 
	ld b,8
comploop  ld a,(de)
          cp (hl)
	  ret nz ; it different
	  inc d
          inc hl
          djnz comploop ; its the same as thisone...
	  xor a
	  ret



store_UDG
	ld b,8
store_UGD_loop
	ld a,(de)
	ld (hl),a
	inc d
	inc hl
	djnz store_UGD_loop

	ret


udgmap2disp
		

		ld hl,$d800
		ld de,$5840
		ld bc,640
		ldir


		ld hl,udg_map

udg2disp
		di
		ld de,$4040
		ld b,192 ; 32 * 6
char_loop1	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop1
		
		ld de,$4800
		ld b,0 ; 32 * 8
char_loop2	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop2

		ld de,$5000
		ld b,192 ; 32 *4
char_loop3	push bc
		push de
		ld a,(hl)
		or a
		call nz,print
		inc hl
		pop de
		inc e
		pop bc
		djnz char_loop3

		ei
		ret

print		push hl
		ld h,0
		ld l,a
		add hl,hl
		add hl,hl
		add hl,hl
		ld bc,udg_store
		add hl,bc
		ld (stack_restore+1), sp
		ld sp,hl 
		ex de,hl
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		inc h
		pop de
		ld (hl),e
		inc h
		ld (hl),d
		ex de,hl
stack_restore		ld sp, 0 ; SMC
			pop hl
			ret




next db 0







	

